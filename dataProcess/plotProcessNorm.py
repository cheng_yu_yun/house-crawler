import properties
from utils import strUtil

def process_plot():
    """ 处理小区信息--规范化 """
    mongo_client = properties.get_mongo_connect()  # 连接mongo
    db_plot = mongo_client.resoldHouse.plotinfo
    plot_infos = db_plot.find()
    for plot_info in plot_infos:
        try:
            id = plot_info['_id']
            # 去掉空格及之后
            p_name = plot_info['p_name']
            p_name = p_name[0:p_name.find(' ')]
            # 去掉 元/m²
            p_per_area_price = plot_info['p_per_area_price']
            if '暂无' in p_per_area_price:
                p_per_area_price = None
            else:
                p_per_area_price = float(p_per_area_price.replace('元/m²',''))
            # 去掉 元/平米/月
            property_price = plot_info['property_price']
            if '暂无' in property_price:
                property_price = None
            else:
                property_price = float(property_price[0:property_price.find('元')])
            # 去掉 m²
            p_total_area = plot_info['p_total_area']
            if '暂无' in p_total_area:
                p_total_area = None
            else:
                p_total_area = float(p_total_area.replace('m²',''))
            # 去掉户，int
            total_house_num = plot_info['total_house_num']
            if '暂无' in total_house_num:
                total_house_num = None
            else:
                total_house_num = int(total_house_num.replace('户',''))
            # int
            park_spot_num = plot_info['park_spot_num']
            if '暂无' in park_spot_num:
                park_spot_num = None
            else:
                park_spot_num = int(park_spot_num)
            # 绿化率分割
            green_rate = plot_info['green_rate']
            green_rate_num = None
            green_rate_level = None
            if '暂无' in green_rate:
                green_rate = None
            else:
                green_rate_num = green_rate[0:green_rate.find("%")]
                green_rate_level = green_rate[green_rate.find("(")+1:green_rate.find(")")]
            property_company = plot_info['property_company']
            if '暂无' in property_company:
                property_company = None
            # 去掉套  int
            second_house_num = plot_info['second_house_num']
            if '暂无' in second_house_num:
                second_house_num = None
            else:
                second_house_num = int(second_house_num.replace('套',''))
            # 去掉套  int
            rent_house_num = plot_info['rent_house_num']
            if '暂无' in rent_house_num:
                rent_house_num = None
            else:
                rent_house_num = int(rent_house_num.replace('套',''))
            # 有逗号分割数组
            p_address = plot_info['p_address']
            if ',' in p_address:
                p_address = p_address.split(',')
            db_plot.update_one({'_id': id}, {'$set': {
                'p_name': p_name,
                'p_per_area_price': p_per_area_price,
                'property_price': property_price,
                'p_total_area': p_total_area,
                'total_house_num': total_house_num,
                'park_spot_num': park_spot_num,
                'green_rate': green_rate,
                'green_rate_num': green_rate_num,
                'green_rate_level': green_rate_level,
                'property_company': property_company,
                'second_house_num': second_house_num,
                'rent_house_num': rent_house_num,
                'p_address': p_address
            }})
        except:
            print("update error")
            print(plot_info)
            print("==" * 50)
    mongo_client.close()  # 关闭mongo

def remove_repeat_plot():
    """ 处理小区信息--去重,依据规范化后的plot_url """
    mongo_client = properties.get_mongo_connect()  # 连接mongo
    db_plot = mongo_client.resoldHouse.plotinfo
    results = db_plot.aggregate([{ '$group': {'_id': '$plot_url', 'count': { '$sum': 1}, "plot_id": {'$first': '$_id'}}},
                        { '$match': {'count': { '$gt': 1}}}
                        ])
    plots = []
    for plot in results:
        plots.append(plot)
    for plot in plots:
        plot_url = plot['_id']
        plot_id = plot['plot_id']
        db_plot.delete_many({'plot_url':plot_url,'_id':{'$ne':plot_id}})
    mongo_client.close()  # 关闭mongo


# process_plot()
# remove_repeat_plot()