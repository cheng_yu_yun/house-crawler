import properties
from utils import strUtil


def process_house():
    """ 处理房屋信息--规范化 """
    mongo_client = properties.get_mongo_connect()  # 连接mongo
    db_house = mongo_client.resoldHouse.houseinfo
    house_infos = db_house.find()
    for house_info in house_infos:
        try:
            id = house_info['_id']  # 更新/删除条件
            # 去掉?及后边   为空时，删除记录（数据爬取失常）
            plot_url = house_info.get('plot_url')
            if plot_url == None:
                db_house.delete_one({'_id':id})
                continue
            elif '?' in plot_url:
                plot_url = plot_url[0:plot_url.find('?')]
            house_url = house_info['house_url']  # 去掉?及后边
            if '?' in house_url:
                house_url = house_url[0:house_url.find('?')]
            h_per_area_price = house_info['h_per_area_price']  # 去掉 元/㎡
            h_per_area_price = float(h_per_area_price.replace('元/㎡',''))
            h_total_area = house_info['h_total_area']  # 去掉 m²
            h_total_area = float(h_total_area.replace('m²',''))
            h_total_price = house_info['h_total_price']  # 去掉 万
            h_total_price = float(h_total_price.replace('万',''))
            # 若包含  '暂无' , 置None     '高层(共7层)'   '共7层'  提取数字-高/中/低
            house_floor = house_info['house_floor']
            house_total_floor = None
            house_floor_level = None
            if house_floor != None:
                if '暂无' in house_floor:
                    house_floor = None
                else:
                    if '(' in house_floor:
                        num = house_floor[house_floor.find('共') + 1:-2]
                        level = house_floor[0]
                        house_floor = num + '-' + level
                        house_total_floor = int(num)
                        house_floor_level = level
                    else:
                        house_floor = house_floor[house_floor.find('共') + 1:-1]
            # 有-1
            elevator = house_info['elevator']
            if elevator !=None and elevator=='有':
                elevator = 1
            # 若包含 '暂无建造年代' , 置None    '’'1995年竣工/普通住宅' 提取时间
            complete_date = house_info['complete_date']
            if complete_date != None:
                if '暂无' in complete_date:
                    complete_date = None
                else:
                    complete_date = int(complete_date[0:complete_date.find('年')])
            # 若包含  '暂无' , 置None '产权年限70年产权' 仅保留数字
            rights_year = house_info['rights_year']
            if rights_year != None:
                if '暂无' in rights_year:
                    rights_year = None
                else:
                    rights_year = int(rights_year.replace('产权年限','').replace('年产权',''))
            book_years = house_info['book_years']  # '房本年限满五年' 提取年数
            if book_years != None and len(book_years)>5:
                book_years =  strUtil.cnum_to_anum(book_years[5])
            only_house = house_info['only_house']
            if only_house != None and only_house == '是':
                only_house = 1
            db_house.update_one({'_id':id},{'$set':{
                'plot_url': plot_url,
                'house_url': house_url,
                'h_per_area_price': h_per_area_price,
                'h_total_area': h_total_area,
                'h_total_price': h_total_price,
                'house_floor': house_floor,
                'complete_date': complete_date,
                'rights_year': rights_year,
                'book_years': book_years,
                'elevator': elevator,
                'only_house': only_house,
                'house_total_floor': house_total_floor,
                'house_floor_level':house_floor_level
            }})
        except:
            print("update error")
            print(house_info)
            print("=="*50)
    mongo_client.close()  # 关闭mongo

def remove_repeat_house():
    """ 处理房屋信息--去重,依据规范化后的house_url """
    mongo_client = properties.get_mongo_connect()  # 连接mongo
    db_house = mongo_client.resoldHouse.houseinfo
    results = db_house.aggregate([{ '$group': {'_id': '$house_url', 'count': { '$sum': 1}, "house_id": {'$first': '$_id'}}},
                        { '$match': {'count': { '$gt': 1}}}
                        ])
    houses = []
    for house in results:
        houses.append(house)
    for house in houses:
        house_url = house['_id']
        house_id = house['house_id']
        db_house.delete_many({'house_url':house_url,'_id':{'$ne':house_id}})
    mongo_client.close()  # 关闭mongo



# process_house()
# remove_repeat_house()