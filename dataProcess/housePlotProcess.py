import properties
from utils import strUtil
import matplotlib.pyplot as plt
import numpy as np

def aggregate_house_district():
    """ 根据区域聚类,获取房屋数量、均价 """
    mongo_client = properties.get_mongo_connect()  # 连接mongo
    mysql_client = properties.get_mysql_connect()  # 连接mysql
    # mysql_cursor = mysql_client.cursor()
    db_house = mongo_client.resoldHouse.houseinfo
    # 根据区域聚类  https://www.jb51.net/article/158276.htm
    district_pipeline = [{'$group': {'_id': "$h_district",
                             'count': {'$sum': 1},
                            'avg': {'$avg': '$h_per_area_price'}
                             }}]
    results = db_house.aggregate(district_pipeline)
    h_districts = []  # x轴
    house_nums = []   # y1
    avg_prices = []   # y2
    for house_group  in results:
        h_district = house_group['_id']
        house_num = house_group['count']
        avg_price = round(house_group['avg'],2)
        h_districts.append(h_district)
        house_nums.append(house_num)
        avg_prices.append(avg_price)
        # query = 'insert into h_district(h_district,house_num, avg_price) values(%s, %s, %s)'
        # values = (h_district,house_num,avg_price)
        # mysql_cursor.execute(query, values)
        # mysql_client.commit()
    mongo_client.close()  # 关闭mongo
    bar_width = 0.4
    # 解决中文显示问题
    plt.rcParams['font.sans-serif'] = ['SimHei']
    plt.rcParams['axes.unicode_minus'] = False
    plt.bar(x=h_districts,height=house_nums,label='房屋总数',
    color='g',alpha=0.8,width=bar_width)

    plt.bar(x=np.arange(0,len(h_districts),1)+bar_width,height=avg_prices,
            label='房屋均价',color='r',alpha=0.8,width=bar_width)
    for x,y in enumerate(house_nums):
        plt.text(x,y + 3000 ,'%s' % y,ha='center',va='top')
    for x,y in enumerate(avg_prices):
        plt.text(x+bar_width,y + 2100,'%s' % y,ha='center',va='top')
    plt.title('区域房屋数量、均价汇总')
    plt.xlabel('区域')
    plt.ylabel('num')
    plt.legend()
    plt.show()


    # properties.close_mysql_connect(mysql_client,mysql_cursor)  # 关闭mysql连接

def aggregate_house_district_street():
    """ 根据区域、街道聚类,获取房屋数量、均价 """
    mongo_client = properties.get_mongo_connect()  # 连接mongo
    mysql_client = properties.get_mysql_connect()  # 连接mysql
    mysql_cursor = mysql_client.cursor()
    db_house = mongo_client.resoldHouse.houseinfo
    # 根据区域聚类  https://www.jb51.net/article/158276.htm
    district_pipeline = [{'$group': {'_id': {'h_district':'$h_district','h_street':'$h_street'},'count': {'$sum': 1},'avg': {'$avg': '$h_per_area_price'}}},
                         ]
    results = db_house.aggregate(district_pipeline)
    for house_group  in results:
        h_district = house_group['_id']['h_district']
        h_street = house_group['_id']['h_street']
        house_num = house_group['count']
        avg_price = round(house_group['avg'],2)
        print(h_district+"--"+h_street+"--"+str(house_num)+"--"+str(avg_price))
        query = 'insert into h_district_street(h_district,h_street,house_num, avg_price) values(%s, %s, %s, %s)'
        values = (h_district,h_street,house_num,avg_price)
        mysql_cursor.execute(query, values)
        mysql_client.commit()
    mongo_client.close()  # 关闭mongo
    properties.close_mysql_connect(mysql_client,mysql_cursor)  # 关闭mysql连接

aggregate_house_district()
# aggregate_house_district_street()