import pymongo
import pymysql

def get_mongo_connect():
    """连接mongo数据库"""
    # mongodb://用户名:密码@IP:端口
    client = pymongo.MongoClient('mongodb://admin:515913@127.0.0.1:27017/')
    # client = pymongo.MongoClient()
    return client

def close_mongo_connect(mongo_client):
    mongo_client.close()


def get_mysql_connect():
    """连接mysql数据库"""
    connect = pymysql.Connect(
        host="127.0.0.1",
        port=3306,
        user="root",
        passwd="515913zc",
        db="housecrawler",
        charset="utf8"
    )
    # cursor = connect.cursor()
    return connect

def close_mysql_connect(client,cursor):
    cursor.close()
    # 关闭数据库连接
    client.close()





