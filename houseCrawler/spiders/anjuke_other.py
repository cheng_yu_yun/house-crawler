import random
import time
import scrapy
from scrapy import  signals
from selenium import webdriver
from utils import strUtil
from houseCrawler.items import PlotCrawlerItem
from utils import fileUtil

"""部分街道--小区页数超过50--需要利用房屋url获取小区信息"""
# 杭州周边-湖州、嘉兴、绍兴
class AnjukePlotSpider(scrapy.Spider):
    name = 'anjuke_other'
    allowed_domains = ['hangzhou.anjuke.com']
    file = open("D:\work_space\Pycharm\house-crawler\static\cyy_plot_part_other.txt","r")
    start_urls = file.readlines()
    file.close()

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(AnjukePlotSpider,cls).from_crawler(crawler,*args,**kwargs)
        spider.chrome = webdriver.Chrome()
        time.sleep(20)
        crawler.signals.connect(spider.spider_closed,signal=signals.spider_closed) # 爬虫关闭
        return spider
    def spider_closed(self,spider):
        spider.chrome.quit()  # selenimu浏览器窗口关闭
        spider.logger.info('Spider closed : %s',spider.name)

    def parse(self, response):
        """解析街道分页页面"""
        # 获取每页所有房屋链接
        house_urls = response.xpath('//section[@class="list"]/div/a/@href').extract()
        for house_url in house_urls:
            try:
                time.sleep(random.randint(2,5))
                yield scrapy.Request(house_url, callback=self.house_parse, dont_filter=True)
            except:
                fileUtil.write_error_url()
                print("house"+house_url)

    def house_parse(self,response):
        """解析房屋页面"""
        # 获取小区链接
        plot_url = response.xpath('//div[@class="community-title-more"]/a/@href').extract_first()
        try:
            if plot_url!=None:
                plot_url = strUtil.remove_spaces(plot_url)
            time.sleep(random.randint(2,5))
            if plot_url!=None:
                yield scrapy.Request(plot_url, callback=self.plot_parse)
                # if "?" in plot_url:
                #     plot_url = plot_url[0:plot_url.find("?")]
        except:
            fileUtil.write_error_url(plot_url)
            print("plot"+plot_url)

    def plot_parse(self,response):
        """解析小区页面"""
        # 获取小区信息，入mongo库
        time.sleep(random.randint(2, 5))
        try:
            item = PlotCrawlerItem()
            p_name = strUtil.remove_spaces(response.xpath('//div[@class="comm-title"]/h1/text()').extract_first())
            item['p_name'] = p_name
            p_per_area_price = strUtil.remove_spaces(response.xpath('//div[@class="price"]/span[@class="average"]/text()').extract_first())  # 小区均价
            item['p_per_area_price'] = p_per_area_price
            dts = response.xpath('//dl[@class="basic-parms-mod"]/dt/text()').extract()
            dds = response.xpath('//dl[@class="basic-parms-mod"]/dd/text()').extract()
            for i in range(len(dts)):
                dt = strUtil.remove_spaces(dts[i])
                dd = strUtil.remove_spaces(dds[i])
                if '物业费' in dt:
                    item['property_price'] = dd     # 物业费
                elif '总建面积' in dt:
                    item['p_total_area'] = dd       # 小区总建面积
                elif '总户数' in dt:
                    item['total_house_num'] = dd    # 总户数
                elif '停车位' in dt:
                    item['park_spot_num'] = dd      # 停车位
                elif '容积率' in dt:
                    item['plot_rate'] = dd          # 容积率
                elif '绿化率' in dt:
                    item['green_rate'] = dd         # 绿化率
                elif '开发商' in dt:
                    item['developer'] = dd          # 开发商
                elif '物业公司' in dt:
                    item['property_company'] = dd   # 物业公司
                elif '周边学校' in dt:
                    item['around_school'] = dd      # 周边学校
                elif '所属商圈' in dt:
                    item['business_circle'] = dd    # 所属商圈
            # 二手房房源数
            second_house_num = strUtil.remove_spaces(response.xpath('//div[@class="houses-sets-mod j-house-num"]/a[@class="num ershou-num"]/text()').extract_first())
            item['second_house_num'] = second_house_num
            # 租房房源数
            rent_house_num = strUtil.remove_spaces(response.xpath('//div[@class="houses-sets-mod j-house-num"]/a[@class="num"]/text()').extract_first())
            item['rent_house_num'] = rent_house_num
            p_address = strUtil.remove_spaces(response.xpath('//div[@class="comm-title"]/h1/span/text()').extract_first())  # 小区地址
            item['p_address'] = p_address
            yield item
        except Exception as e:
            url = response.request.url
            fileUtil.write_error_url(url)
            print("get plot info error")

