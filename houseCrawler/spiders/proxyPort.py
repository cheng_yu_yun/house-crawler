import random
import time
import scrapy

from utils import proxyUtil

from scrapy import  signals
from selenium import webdriver

class ProxyPortSpider(scrapy.Spider):
    name = 'proxyport'
    allowed_domains = ['ip.jiangxianli.com']
    start_urls = ['https://www.kuaidaili.com/free/inha/{}/'.format(i) for i in range(1,20)]

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(ProxyPortSpider, cls).from_crawler(crawler, *args, **kwargs)
        spider.chrome = webdriver.Chrome()
        time.sleep(20)
        crawler.signals.connect(spider.spider_closed, signal=signals.spider_closed)  # 爬虫关闭
        return spider

    def spider_closed(self, spider):
        spider.chrome.quit()  # selenimu浏览器窗口关闭
        spider.logger.info('Spider closed : %s', spider.name)

    def parse(self, response):
        """解析快代理  主页面"""
        proxys = response.xpath('//tbody/tr/td[position()<3]/text()').extract()
        file = open('D:\work_space\Pycharm\house-crawler\houseCrawler\static\ip.txt', mode='a')
        global i
        for i in range(len(proxys)):
            proxy = proxys[i]+":"+proxys[i+1]
            if proxyUtil.verify_proxy(proxy) == True:
                file.write(proxy + "\n")
                print(proxy + "有效")
                yield {
                    "type": "proxy",
                    "proxy_source": "jiangxianli",
                    "ip_port": proxy

                }
            else:
                print(proxy + "无效")
            i+=1
        file.close()
        time.sleep(random.random() * 5)


