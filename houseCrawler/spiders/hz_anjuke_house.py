import random
import time

import scrapy
from scrapy import  signals
from selenium import webdriver
from utils import strUtil
from houseCrawler.items import PlotCrawlerItem
from houseCrawler.items import HousecrawlerItem
from utils import fileUtil
import logging

class AnjukePlotSpider(scrapy.Spider):
    name = 'anjuke_house'
    allowed_domains = ['hangzhou.anjuke.com']
    file = open("D:\work_space\Pycharm\house-crawler\static\cyy_house_part1.txt","r")
    start_urls = file.readlines()
    file.close()

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(AnjukePlotSpider,cls).from_crawler(crawler,*args,**kwargs)
        spider.chrome = webdriver.Chrome()
        time.sleep(20)
        crawler.signals.connect(spider.spider_closed,signal=signals.spider_closed) # 爬虫关闭
        return spider
    def spider_closed(self,spider):
        spider.chrome.quit()  # selenimu浏览器窗口关闭
        spider.logger.info('Spider closed : %s',spider.name)

    def parse(self, response):
        # 获取每页所有房屋链接
        house_urls = response.xpath('//section[@class="list"]/div/a/@href').extract()
        for house_url in house_urls:
            try:
                time.sleep(random.randint(2,5))
                yield scrapy.Request(house_url, callback=self.house_parse, dont_filter=True)
            except:
                fileUtil.write_error_url(house_url)
                print("house"+house_url)

    def house_parse(self,response):
        # 房屋链接
        house_url = response.url
        try:
            # 房屋单位面积价格
            h_per_area_price = response.xpath('//div[@class="maininfo-avgprice-price"]/text()').extract_first()
            # 房屋总面积
            h_total_area = response.xpath(
                '//div[@class="maininfo-model-item maininfo-model-item-2"]/div/i/text()').extract_first()
            # 总价
            h_total_price = response.xpath('//span[@class="maininfo-price-num"]/text()').extract_first()
            # 户型
            house_type = response.xpath(
                '//div[@class="maininfo-model-item maininfo-model-item-1"]/div[@class="maininfo-model-strong"]').xpath(
                "string(.)").extract_first()
            # 房屋种类 （公寓、住宅）
            house_sort = response.xpath(
                '//div[@id="houseInfo"]//tr[1]/td[3]/span[@class="houseInfo-main-item-name"]/text()').extract_first()
            # 楼层
            house_floor = response.xpath(
                '//div[@class="maininfo-model-item maininfo-model-item-1"]/div[@class="maininfo-model-weak"]/text()').extract_first()
            # 朝向
            orientation = response.xpath(
                '//div[@class="maininfo-model-item maininfo-model-item-3"]/div[@class="maininfo-model-strong"]/i/text()').extract_first()
            # 装修
            decorate_type = response.xpath(
                '//div[@class="maininfo-model-item maininfo-model-item-2"]/div[@class="maininfo-model-weak"]/text()').extract_first()
            # 电梯
            elevator = "没有"
            labels = response.xpath('//div[@class="maininfo-tags"]/span[@class="maininfo-tags-item"]').xpath(
                "string(.)").extract()
            for label in labels:
                if label == "电梯":
                    elevator = "有"
            # 竣工时间
            complete_date = response.xpath(
                '//div[@class="maininfo-model-item maininfo-model-item-3"]/div[@class="maininfo-model-weak"]/text()').extract_first().split(
                "/")[0]
            # 所属小区
            plot_name = strUtil.remove_spaces(response.xpath(
                '//div[@class="maininfo-community-item"]/a[@class="anchor anchor-weak"]/text()').extract_first())
            # 所属区
            h_district = response.xpath(
                '//div[@class="maininfo-community"]/div[@class="maininfo-community-item"][2]/span['
                '@class="maininfo-community-item-name"]/a[1]').xpath(
                "string(.)").extract_first().strip()
            # 所属街道
            h_street = response.xpath(
                '//div[@class="maininfo-community"]/div[@class="maininfo-community-item"][2]/span['
                '@class="maininfo-community-item-name"]/a[2]').xpath(
                "string(.)").extract_first().strip()
            # 产权年限
            rights_year = response.xpath(
                '//div[@id="houseInfo"]//tr[2]/td[2]/span[@class="houseInfo-main-item-name"]/text()').extract_first()
            # 房本年限
            book_years = response.xpath(
                '//div[@id="houseInfo"]//tr[2]/td[3]/span[@class="houseInfo-main-item-name"]/text()').extract_first()
            # 唯一住房
            only_house = response.xpath(
                '//div[@id="houseInfo"]//tr[3]/td[2]/span[@class="houseInfo-main-item-name"]/text()').extract_first()
            item = HousecrawlerItem()
            item['h_per_area_price'] = h_per_area_price
            item['h_total_area'] = h_total_area
            item['h_total_price'] = h_total_price
            item['house_type'] = house_type
            item['house_sort'] = house_sort
            item['house_floor'] = house_floor
            item['orientation'] = orientation
            item['decorate_type'] = decorate_type
            item['elevator'] = elevator
            item['complete_date'] = complete_date
            item['plot_name'] = plot_name
            item['h_district'] = h_district
            item['h_street'] = h_street
            item['rights_year'] = rights_year
            item['book_years'] = book_years
            item['only_house'] = only_house
            item['house_url'] = house_url
            yield item
        except:
            fileUtil.write_error_house_url(house_url)
            print("get street info error"+house_url)


