import scrapy
class KuaidailiSpider(scrapy.Spider):
    name = 'kuaidaili'
    allowed_domains = ['kuaidaili.com']
    start_urls = ['https://www.kuaidaili.com/free/']

    def parse(self,response):
        # 获取总页数
        page_total=response.xpath('//div[@id="listnav"]//li[last()-1]/a/text()').extract_first()
        base_url="https://www.kuaidaili.com/free/inha/{}/"
        for num in range(1,int(5)):
          yield scrapy.Request(base_url.format(num+1), callback=self.ipparse, dont_filter=True)

    def ipparse(self,response):
        ip_totals = response.xpath('// div[ @ id = "list"] // tr / td[1]/text()').extract()
        port_totals = response.xpath('// div[ @ id = "list"] // tr / td[2]/text()').extract()
        type_totals = response.xpath('// div[ @ id = "list"] // tr / td[4]/text()').extract()
        url = "https://hangzhou.anjuke.com/sale/binjiangb-q-bmhhz/p1/"
        for type,ip,port in zip(type_totals,ip_totals,port_totals) :
            proxy = type+'://' + ip + ':' + port
            yield scrapy.Request(url, callback=self.effectiveIpparse, errback=self.errback,dont_filter=True,meta={'proxy':proxy})


    def effectiveIpparse(self,response):
            yield {
                "proxy":response.meta["proxy"]
            }
            print(response.meta["proxy"])

    def errback(self,response):
        print("ip不可用："+response.request.meta["proxy"])