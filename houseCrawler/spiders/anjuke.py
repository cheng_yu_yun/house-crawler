import scrapy


class AnjukeSpider(scrapy.Spider):
    name = 'anjuke'
    allowed_domains = ['km.anjuke.com']
    start_urls = ['https://km.anjuke.com/sale/chenggongqu/p{}/?from=navigation'.format(num) for num in range(1,2)]

    def parse(self, response):
        #解析主页面
        houseurls=response.xpath('//div[@class="property"]/a/@href').extract()
        for url in houseurls:
            yield scrapy.Request(url, callback=self.houseparse, dont_filter=True)

    def houseparse(self, response):
        #爬取每个房屋的数据
        totalprice = response.xpath('//div[@class="maininfo-price-wrap"]').xpath("string(.)").extract_first()
        unitprice = response.xpath('//div[@class="maininfo-avgprice-price"]/text()').extract_first()
        housetype = response.xpath('//div[@class="maininfo-model-item maininfo-model-item-1"]//div[@class="maininfo-model-strong"]').xpath("string(.)").extract_first()
        yield {
            "totalprice": totalprice,
            "unitprice": unitprice,
            "housetype": housetype
        }