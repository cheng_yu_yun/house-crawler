# Define here the models for your spider middleware
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/spider-middleware.html
import random
import time

from scrapy import signals

# useful for handling different item types with a single interface
from itemadapter import is_item, ItemAdapter
from houseCrawler.settings import USER_AGENTS
from random import choice
from scrapy.http import HtmlResponse

class UaMiddlewares(object):
    def process_request(self, request, spider):
        user_agent = choice(USER_AGENTS)
        # print(user_agent)
        request.headers.setdefault(b'User-Agent', user_agent)
        # selenium
        url = request.url
        spider.chrome.get(url)
        html = spider.chrome.page_source
        time.sleep(random.randint(4,9))
        return HtmlResponse(url=url,body=html,request=request,encoding='utf-8')

class ProxyMiddlewares(object):
    def process_request(self, request, spider):
        request.meta['proxy'] = "http://39.100.106.237:8002"
