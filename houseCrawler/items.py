# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy

class HousecrawlerItem(scrapy.Item):
    """房屋属性"""
    h_per_area_price = scrapy.Field()  # 房屋单位面积价格
    h_total_area = scrapy.Field()  # 房屋总面积
    h_total_price = scrapy.Field()  # 总价
    house_type = scrapy.Field()  # 户型
    house_sort = scrapy.Field()  # 房屋种类 （公寓、住宅）
    floor = scrapy.Field()  # 楼层
    orientation = scrapy.Field()  # 朝向
    decorate_type = scrapy.Field()  # 装修
    elevator = scrapy.Field()  # 电梯
    complete_date = scrapy.Field()  # 竣工时间
    plot_name = scrapy.Field()  # 所属小区
    h_district = scrapy.Field()  # 所属区
    h_street = scrapy.Field()  # 所属街道
    rights_year = scrapy.Field()  # 产权年限
    book_years = scrapy.Field()  # 房本年限
    only_house = scrapy.Field()  # 唯一住房
    p_per_area_price = scrapy.Field()  # 小区均价
    property_price = scrapy.Field()  # 物业费
    plot_rate = scrapy.Field()  # 容积率
    green_rate = scrapy.Field()  # 绿化率
    house_url = scrapy.Field()  # 房屋链接

class PlotCrawlerItem(scrapy.Item):
    """小区属性"""
    p_name = scrapy.Field()             # 小区名称
    p_per_area_price = scrapy.Field()   # 小区均价
    property_price = scrapy.Field()     # 物业费
    p_total_area = scrapy.Field()       # 小区总建面积
    total_house_num = scrapy.Field()    # 总户数
    park_spot_num = scrapy.Field()      # 停车位
    plot_rate = scrapy.Field()          # 容积率
    green_rate = scrapy.Field()         # 绿化率
    developer = scrapy.Field()          # 开发商
    property_company = scrapy.Field()   # 物业公司
    around_school = scrapy.Field()      # 周边学校
    business_circle = scrapy.Field()    # 所属商圈
    second_house_num = scrapy.Field()   # 二手房房源数
    rent_house_num = scrapy.Field()     # 租房房源数
    p_address = scrapy.Field()          # 小区地址

