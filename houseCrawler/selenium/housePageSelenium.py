# coding = utf-8
"""获取每一页街道链接"""
import random
import time

from selenium import webdriver
from utils import strUtil

def init():
    # 使用selenium
    opt = webdriver.ChromeOptions()
    opt .add_argument("–proxy-server=http://202.162.211.242:443")# 一定要注意，等号两边不能有空格
    web = webdriver.Chrome()
    web.get('https://hangzhou.anjuke.com/sale/?from=navigation')
    time.sleep(20)
    return web

def click_district(web):
    """点击地区"""
    districts = web.find_elements_by_xpath('//ul[@class="region region-line2"]/li[@tongji_tag !=""]/a')
    district_urls = []
    for district in districts :
        district_url = district.get_attribute("href")
        district_urls.append(district_url)
    for district_url in district_urls:
        time.sleep(random.random() * 3)
        web.get(district_url)  #访问地区链接
        click_street_start(web)
    web.close()


def click_street_start(web):
    """点击街道"""
    streets = web.find_elements_by_xpath('//ul[@class="region region-line3"]/li[@tongji_tag !=""]/a')
    street_urls = []
    for street in streets:
        street_url = street.get_attribute("href")
        street_urls.append(street_url)
    for street_url in street_urls:
        time.sleep(random.random() * 5)
        web.get(street_url)  #访问街道链接
        click_next_page(web)

def click_next_page(web):
    file = open("D:\work_space\Pycharm\house-crawler\static\street_page_urls.txt","a")
    file.write(web.current_url+"\n")
    file.close()
    try:
        next_page = web.find_element_by_xpath('//div[@class="pagination"]/a[@class="next next-active"]')
        if next_page != None:
            time.sleep(random.random() * 3)
            try:
                next_page = web.find_element_by_xpath('//div[@class="pagination"]/a[@class="next next-active"]')
                # next = next_page.click()
                # web.get(next)
                next_page.click()
            except:
                next_page = web.find_element_by_xpath('//div[@class="pagination"]/a[@class="next next-active"]')
                next_page.click()
            street_page_url = web.current_url
            click_next_page(web)
    except:
        return



# 运行入口函数
click_district(init())