# coding = utf-8
import random
import time

from selenium import webdriver

import properties
from utils import strUtil

"""爬取房屋信息"""

def init():
    """ 初始化浏览器连接 """
    # 使用selenium
    option = webdriver.ChromeOptions()
    option.add_argument("–proxy-server=http://120.220.220.95:8085")# 一定要注意，等号两边不能有空格
    web = webdriver.Chrome(options=option)
    web.get('https://hangzhou.anjuke.com/sale/?from=navigation')
    time.sleep(10)
    return web

def click_district(web,mongo_client):
    """ 点击地区 """
    # 淳安  over
    # 建德  over
    # 拱墅  over
    # 上城  over
    # 萧山  over
    # 桐庐  over
    # 杭州周边  over
    # 余杭  over
    # 临安  青山湖  over
    # 临平  临平
    # //ul[@class="region region-line2"]/li[(position()>=4 and position()<=6) or position()>=14]/a
    districts = web.find_elements_by_xpath('//ul[@class="region region-line2"]/li[position()=8]/a')
    district_urls = []
    for district in districts :
        district_url = district.get_attribute("href")
        district_urls.append(district_url)
    for district_url in district_urls:
        try:
            web.get(district_url)  # 访问地区链接
            captcha_verify(web, district_url)  # 防止图形验证，再次访问
            time.sleep(random.random() * 3 + 2)
            click_street_start(web,mongo_client)
        except:
            write_error_house_url(district_url)
            print("parse district error  "+district_url)
    web.close()


def click_street_start(web,mongo_client):
    """点击街道"""
    streets = web.find_elements_by_xpath('//ul[@class="region region-line3"]/li[position()=5]/a')
    street_urls = []
    for street in streets:
        street_url = street.get_attribute("href")
        street_urls.append(street_url)
    for street_url in street_urls:
        try:
            web.get(street_url)  # 访问街道链接
            captcha_verify(web,street_url)  # 防止图形验证，再次访问
            time.sleep(random.random() * 3 + 2)
            click_next_page(web,mongo_client,1)  # 点击下一页
        except:
            write_error_house_url(street_url)
            print("parse street error  "+street_url)


def click_next_page(web,mongo_client,page_num):
    """ 点击下一页 """
    locate_house_pagination(web)
    print("current page is page " + str(page_num))
    parse_street_page(web,mongo_client)
    try:
        # 获取下一页css
        next_page = web.find_element_by_xpath('//div[@class="pagination"]/a[@class="next next-active"]')
        if next_page != None:
            try:
                next_page = web.find_element_by_xpath('//div[@class="pagination"]/a[@class="next next-active"]')
                next_page.click()
            except:
                # 防止页面改变，重新获取再点击
                next_page = web.find_element_by_xpath('//div[@class="pagination"]/a[@class="next next-active"]')
                next_page.click()
                print("click next page error")
            time.sleep(random.random() * 3 + 2)
            page_num += 1
            click_next_page(web,mongo_client,page_num)
        else:
            print("page pass all")
    except:
        print("page error or page pass all and current page is page " + str(page_num))
        print("please check it by yourself")
        print("==" * 100)



def parse_street_page(web,mongo_client):
    """ 获取所有房屋css """
    houses = web.find_elements_by_xpath('//section[@class="list"]/div[@class="property"]/a')  # 获取所有房屋css
    houses_div = web.find_elements_by_xpath('//section[@class="list"]/div[@class="property"]/a/div[@class="property-content"]')
    i = 0
    while i < len(houses):
        house_url = houses[i].get_attribute("href")
        try:
            current_house = houses_div[i]
            current_house.click()  # 点击房屋，跳转房屋详情页面
            time.sleep(random.random() * 3 + 1)
            if 'captcha-verify' in web.current_url:
                time.sleep(random.random() * 3 + 3)  # 图形验证
                windows = web.window_handles
                handle_num = len(windows)
                if handle_num >= 2:
                    web.close()
                windows = web.window_handles
                web.switch_to.window(windows[0])     # 定位到房屋分页的页面
                i += 1
                continue
            i += 1
            parse_house_html(web, mongo_client)  # 解析房屋页面数据
        except:
            locate_house_pagination(web)
            write_error_house_url(house_url)
            print("parse_street_page() get house info error " + house_url)

def parse_house_html(web,mongo_client):
    """ 解析房屋页面，获取数据 """
    item = {}
    windows = web.window_handles  # 移动句柄为当前页面
    handle_num = len(windows)     # 获取当前窗口数量
    if handle_num >= 2:
        web.switch_to.window(windows[-1])  # 定位到新打开的页面--房屋信息页面
    elif handle_num == 1:         # 仅有房屋分页页面
        return
    house_url = web.current_url
    # 房屋链接
    item['house_url'] = house_url
    try:
        # 房屋单位面积价格
        h_per_area_price = strUtil.remove_spaces(web.find_element_by_xpath('//div[@class="maininfo-avgprice"]/div[@class="maininfo-avgprice-price"]').text)
        item['h_per_area_price'] = h_per_area_price
        # 房屋总面积
        h_total_area = strUtil.remove_spaces(web.find_element_by_xpath('//div[@class="maininfo-model-item maininfo-model-item-2"]/div/i').text) + "m²"
        item['h_total_area'] = h_total_area
        # 总价
        h_total_price = strUtil.remove_spaces(web.find_element_by_xpath('//span[@class="maininfo-price-num"]').text) + "万"
        item['h_total_price'] = h_total_price
        # 户型
        house_type = strUtil.remove_spaces(web.find_element_by_xpath('//div[@class="maininfo-model-item maininfo-model-item-1"]/div[@class="maininfo-model-strong"]').text)
        item['house_type'] = house_type
        # 房屋种类 （公寓、住宅）
        house_sort = strUtil.remove_spaces(web.find_element_by_xpath('//div[@id="houseInfo"]//tr[1]/td[3]/span[@class="houseInfo-main-item-name"]').text)
        item['house_sort'] = house_sort
        # 楼层
        house_floor = strUtil.remove_spaces(web.find_element_by_xpath('//div[@class="maininfo-model-item maininfo-model-item-1"]/div[@class="maininfo-model-weak"]').text)
        item['house_floor'] = house_floor
        # 朝向
        orientation = strUtil.remove_spaces(web.find_element_by_xpath('//div[@class="maininfo-model-item maininfo-model-item-3"]/div[@class="maininfo-model-strong"]/i').text)
        item['orientation'] = orientation
        # 装修
        decorate_type = strUtil.remove_spaces(web.find_element_by_xpath('//div[@class="maininfo-model-item maininfo-model-item-2"]/div[@class="maininfo-model-weak"]').text)
        item['decorate_type'] = decorate_type
        # 电梯
        elevator = None
        try:
            spans = web.find_elements_by_xpath('//div[@class="maininfo-tags"]/span[@class="maininfo-tags-item"]')
            for span in spans:
                text = strUtil.remove_spaces(span.text)
                if text == '电梯':
                    elevator = "有"
                    break
        except:
            text = strUtil.remove_spaces(web.find_element_by_xpath('//div[@class="maininfo-tags"]/span[@class="maininfo-tags-item"]').text)
            if text == '电梯':
                elevator = "有"
        item['elevator'] = elevator
        # 竣工时间
        complete_date = strUtil.remove_spaces(web.find_element_by_xpath('//div[@class="maininfo-model-item maininfo-model-item-3"]/div[@class="maininfo-model-weak"]').text)
        item['complete_date'] = complete_date
        # 所属小区
        plot_name = strUtil.remove_spaces(web.find_element_by_xpath('//div[@class="maininfo-community-item"]/a[@class="anchor anchor-weak"]').text)
        item['plot_name'] = plot_name
        # 所属区
        h_district = strUtil.remove_spaces(web.find_element_by_xpath('//div[@class="maininfo-community"]/div[@class="maininfo-community-item"][2]/span[@class="maininfo-community-item-name"]/a[1]').text)
        item['h_district'] = h_district
        # 所属街道
        h_street = strUtil.remove_spaces(web.find_element_by_xpath('//div[@class="maininfo-community"]/div[@class="maininfo-community-item"][2]/span[@class="maininfo-community-item-name"]/a[2]').text)
        item['h_street'] = h_street
        house_main_infos = web.find_elements_by_xpath('//tbody[@class="houseInfo-main"]/tr[position()<5]/td[position()>1]')
        # 产权年限
        rights_year = None
        # 房本年限
        book_years = None
        # 唯一住房
        only_house = None
        for house_main_info in house_main_infos:
            info_text = strUtil.remove_spaces(house_main_info.text)
            if '产权年限' in info_text:
                rights_year = info_text
            elif '房本年限' in info_text:
                book_years = info_text
            elif '唯一住房' in info_text:
                only_house = '是'

        item['rights_year'] = rights_year
        item['book_years'] = book_years
        item['only_house'] = only_house
        # 小区链接
        plot_url = web.find_element_by_xpath('//div[@class="community-title-more"]/a').get_attribute('href')
        item['plot_url'] = plot_url
        web.close()                       # 关闭房屋信息页面
        web.switch_to.window(windows[0])  # 定位到房屋分页页面
        time.sleep(random.random() * 2)
    except:
        locate_house_pagination(web)
        write_error_house_url(house_url)
        print("parse_house_html() get house info error " + house_url)
        print("error house info is ")
        print(item)
        print("-" * 100)
    # 成功插入全部数据，不成功至少插入房屋链接
    mongo_client.resoldHouse.houseinfo.insert_one(item)
    print(item)

def write_error_house_url(url):
    file = open("D:\work_space\Pycharm\house-crawler\static\error_house_url.txt", "a")
    file.write(url + "\n")
    file.close()

def captcha_verify(web,ori_url):
    if 'captcha-verify' in web.current_url:
        time.sleep(random.random() * 3 + 5)
        web.get(ori_url)  # 图形验证--重新访问链接

def locate_house_pagination(web):
    """定位到房屋分页页面"""
    windows = web.window_handles  # 获取当前浏览器所有句柄
    handle_num = len(windows)     # 获取当前窗口数量
    if handle_num >= 2:           # 打开房屋信息页面
        web.switch_to.window(windows[-1])  # 定位到房屋信息页面
        web.close()                        # 关闭房屋信息页面
        print("close current house info ")
    web.switch_to.window(windows[0])       # 定位到房屋分页的页面

""" 运行入口函数 """
mongo_client = properties.get_mongo_connect()  # 连接mongo
click_district(init(),mongo_client)
mongo_client.close()  # 关闭mongo