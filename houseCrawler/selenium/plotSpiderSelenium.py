# coding = utf-8
import random
import time

from selenium import webdriver

import properties
from utils import strUtil

"""爬取小区信息"""

def init():
    """ 初始化浏览器连接 """
    # 使用selenium
    option = webdriver.ChromeOptions()
    option.add_argument("–proxy-server=http://120.220.220.95:8085")# 一定要注意，等号两边不能有空格
    web = webdriver.Chrome(options=option)
    web.get('https://hangzhou.anjuke.com/community/?from=navigation')
    time.sleep(10)
    return web

def click_district(web,mongo_client):
    """点击地区"""
    districts = web.find_elements_by_xpath('//ul[@class="region-parents"]/li[position()>1]/a')
    district_urls = []
    for district in districts :
        district_url = district.get_attribute("href")
        district_urls.append(district_url)
    for district_url in district_urls:
        try:
            web.get(district_url)  # 访问地区链接
            captcha_verify(web, district_url)  # 防止图形验证，再次访问
            time.sleep(random.random() * 3)
            click_street_start(web,mongo_client)
        except:
            write_error_plot_url(district_url)
            print("parse district error  "+district_url)
    web.close()


def click_street_start(web,mongo_client):
    """点击街道"""
    streets = web.find_elements_by_xpath('//div[@class="region-childs"]/li[position()>1]/a')
    street_urls = []
    for street in streets:
        street_url = street.get_attribute("href")
        street_urls.append(street_url)
    for street_url in street_urls:
        try:
            web.get(street_url)  # 访问街道链接
            captcha_verify(web,street_url)  # 防止图形验证，再次访问
            time.sleep(random.random() * 5)
            click_next_page(web,mongo_client)  # 点击下一页
        except:
            write_error_plot_url(street_url)
            print("parse street error  "+street_url)


def click_next_page(web,mongo_client):
    """ 点击下一页 """
    parse_plot_page(web,mongo_client)
    try:
        # 获取下一页css
        next_page = web.find_element_by_xpath('//div[@class="pagination page-bar"]/a[@class="next next-active"]')
        if next_page != None:
            time.sleep(random.random() * 3)
            try:
                next_page = web.find_element_by_xpath('//div[@class="pagination page-bar"]/a[@class="next next-active"]')
                next_page.click()
            except:
                # 防止页面改变，重新获取再点击
                next_page = web.find_element_by_xpath('//div[@class="pagination page-bar"]/a[@class="next next-active"]')
                next_page.click()
                print("click next page error")
            click_next_page(web,mongo_client)
    except:
        print("page pass all")


def parse_plot_page(web,mongo_client):
    """ 获取所有小区css """
    plot_page_url = web.current_url
    plots = web.find_elements_by_xpath('//div[@class="list-cell"]/a')  # 获取所有小区css
    i = 0
    while i<len(plots):
        plots = web.find_elements_by_xpath('//div[@class="list-cell"]/a')  # 获取所有小区css，由于回退，需要重新获取
        plot_url = plots[i].get_attribute("href")
        try:
            # plots = web.find_elements_by_xpath('//div[@class="list-cell"]/a')
            current_plot = plots[i]
            current_plot.click()  # 点击小区，跳转小区详情页面
            if 'captcha-verify' in web.current_url:
                time.sleep(random.random() * 3 + 5)  # 图形验证
                web.back()  # 回退上一页
                continue
            i += 1
            time.sleep(random.random() * 3 + 3)
            parse_plot_html(web, mongo_client)  # 解析小区页面数据
            web.back()  # 回退上一页，
            time.sleep(random.random() * 3 + 2)
        except:
            write_error_plot_url(plot_url)
            print("get plot URL error " + plot_url)

    # for i in range(len(plots)):
    #     plot_url = plots[1].get_attribute("href")
    #     try:
    #         plots = web.find_elements_by_xpath('//div[@class="list-cell"]/a')
    #         current_plot = plots[i]
    #         current_plot.click()
    #         if 'captcha-verify' in web.current_url:
    #             time.sleep(random.random() * 3 + 5)  # 图形验证
    #             i -= 1
    #             web.back()  # 回退上一页
    #         else:
    #             time.sleep(random.random() * 3 + 3)
    #             parse_plot_html(web, mongo_client)  # 解析小区页面数据
    #             web.back()  # 回退上一页
    #             time.sleep(random.random() * 3 + 2)
    #     except:
    #         write_error_plot_url(plot_url)
    #         print("get plot URL error " + plot_url)

def parse_plot_html(web,mongo_client):
    """ 解析小区页面，获取数据 """
    plot_url = web.current_url
    try:
        item = {}
        item["plot_url"] = plot_url
        p_name = strUtil.remove_spaces(web.find_element_by_xpath('//div[@class="comm-title"]/h1').text)
        item['p_name'] = p_name
        p_per_area_price = strUtil.remove_spaces(web.find_element_by_xpath('//div[@class="price"]/span[@class="average"]').text)  # 小区均价
        item['p_per_area_price'] = p_per_area_price
        dts = web.find_elements_by_xpath('//dl[@class="basic-parms-mod"]/dt')
        dds = web.find_elements_by_xpath('//dl[@class="basic-parms-mod"]/dd')
        for i in range(len(dts)):
            dt = strUtil.remove_spaces(dts[i].text)
            dd = strUtil.remove_spaces(dds[i].text)
            if '物业费' in dt:
                item['property_price'] = dd  # 物业费
            elif '总建面积' in dt:
                item['p_total_area'] = dd  # 小区总建面积
            elif '总户数' in dt:
                item['total_house_num'] = dd  # 总户数
            elif '停车位' in dt:
                item['park_spot_num'] = dd  # 停车位
            elif '容积率' in dt:
                item['plot_rate'] = dd  # 容积率
            elif '绿化率' in dt:
                item['green_rate'] = dd  # 绿化率
            elif '开发商' in dt:
                item['developer'] = dd  # 开发商
            elif '物业公司' in dt:
                item['property_company'] = dd  # 物业公司
            elif '周边学校' in dt:
                item['around_school'] = dd  # 周边学校
            elif '所属商圈' in dt:
                item['business_circle'] = dd  # 所属商圈
        # 二手房房源数
        second_house_num = strUtil.remove_spaces(
            web.find_element_by_xpath('//div[@class="houses-sets-mod j-house-num"]/a[@class="num ershou-num"]').text)
        item['second_house_num'] = second_house_num
        # 租房房源数
        rent_house_num = strUtil.remove_spaces(web.find_element_by_xpath('//div[@class="houses-sets-mod j-house-num"]/a[@class="num"]').text)
        item['rent_house_num'] = rent_house_num
        p_address = strUtil.remove_spaces(web.find_element_by_xpath('//div[@class="comm-title"]/h1/span').text)  # 小区地址
        item['p_address'] = p_address
        mongo_client.resoldHouse.plotinfo.insert_one(item)
        print(item)
    except:
        write_error_plot_url(plot_url)
        print("get plot info error" + plot_url)

def write_error_plot_url(url):
    file = open("D:\work_space\Pycharm\house-crawler\static\error_plot_url.txt", "a")
    file.write(url + "\n")
    file.close()

def captcha_verify(web,ori_url):
    if 'captcha-verify' in web.current_url:
        time.sleep(random.random() * 3 + 5)
        web.get(ori_url)  # 图形验证--重新访问链接


""" 运行入口函数 """
mongo_client = properties.get_mongo_connect()  # 连接mongo
click_district(init(),mongo_client)
mongo_client.close()  # 关闭mongo