# coding = utf-8
"""获取每一页街道链接"""
import random
import time

from selenium import webdriver
from utils import strUtil

def init():
    # 使用selenium
    opt = webdriver.ChromeOptions()
    opt .add_argument("–proxy-server=http://202.162.211.242:443")# 一定要注意，等号两边不能有空格
    web = webdriver.Chrome()
    web.get('https://hangzhou.anjuke.com/community/?from=navigation')
    time.sleep(20)
    return web

def click_district(web):
    """点击地区"""
    districts = web.find_elements_by_xpath('//ul[@class="region-parents"]/li[position()>1]/a')
    district_urls = []
    for district in districts :
        district_url = district.get_attribute("href")
        district_urls.append(district_url)
    for district_url in district_urls:
        time.sleep(random.random() * 3)
        web.get(district_url)  #访问地区链接
        click_street_start(web)
    web.close()


def click_street_start(web,mongo_client):
    """点击街道"""
    streets = web.find_elements_by_xpath('//div[@class="region-childs"]/li[position()>1]/a')
    street_urls = []
    for street in streets:
        street_url = street.get_attribute("href")
        street_urls.append(street_url)
    for street_url in street_urls:
        try:
            web.get(street_url)  #访问街道链接
            captcha_verify(web,street_url)
            time.sleep(random.random() * 5)
            click_next_page(web)
        except:
            write_error_plot_url(street_url)

def click_next_page(web):
    file = open("D:\work_space\Pycharm\house-crawler\static\plot_page_urls.txt","a")
    file.write(web.current_url+"\n")
    file.close()
    try:
        next_page = web.find_element_by_xpath('//div[@class="pagination page-bar"]/a[@class="next next-active"]')
        if next_page != None:
            time.sleep(random.random() * 3)
            try:
                next_page = web.find_element_by_xpath('//div[@class="pagination page-bar"]/a[@class="next next-active"]')
                next_page.click()
            except:
                next_page = web.find_element_by_xpath('//div[@class="pagination page-bar"]/a[@class="next next-active"]')
                next_page.click()
            plot_page_url = web.current_url
            click_next_page(web)
    except:
        return

def captcha_verify(web,ori_url):
    if 'captcha-verify' in web.current_url:
        time.sleep(random.random() * 3 + 5)
        web.get(ori_url)  # 图形验证--重新访问链接

def write_error_plot_url(url):
    file = open("D:\work_space\Pycharm\house-crawler\static\error_plot_url.txt", "a")
    file.write(url + "\n")
    file.close()


# 运行入口函数
click_district(init())