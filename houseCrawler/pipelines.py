# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
import pymongo
import properties
from houseCrawler.items import PlotCrawlerItem

class HousecrawlerPipeline(object):
    def open_spider(self,spider):
        #进行mongodb的连接
        self.mongo_client = properties.get_mongo_connect()
        # 进行mysql连接
        self.mysql_client = properties.get_mysql_connect()
        self.mysql_cursor = self.mysql_client.cursor()


    def process_item(self, item, spider):
        if type(item) == PlotCrawlerItem:
            self.handle_plot(item)
        elif type(item) ==  HousecrawlerItem:
            self.handle_house(item)
        elif item["type"] == 'proxy':
            self.handle_proxy(item)

        return item

    def close_spider(self,spider):
        #关闭mongo的连接
        properties.close_mongo_connect(self.mongo_client)
        # 关闭mysql连接
        properties.close_mysql_connect(self.mysql_client,self.mysql_cursor)


    def handle_house(self,item):
        house_info = dict(item)  # 将item变成字典形式
        # 进行Mongo数据的插入
        self.mongo_client.resoldHouse.house.insert_one(house_info)
        print("house insert successful--"+house_info['house_url'])

    def handle_plot(self,item):
        """将小区数据插入"""
        plot_info = dict(item)  # 将item变成字典形式
        self.mongo_client.resoldHouse.plotinfo.insert_one(plot_info)
        print("plot insert successful--"+plot_info['p_name'])

    def handle_proxy(self,item):
        proxy = item['ip_port']
        sql = "INSERT INTO proxy VALUES(%s)"
        # 这里是元组数据，(str,str,str,str)
        try:
            self.mysql_cursor.execute(sql, proxy)
            self.mysql_client.commit()
        except:
            print("已存在")

