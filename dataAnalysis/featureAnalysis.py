import math
import re
import matplotlib.pyplot as plt
import pymongo
import numpy as np
import pandas as pd
import seaborn as sns
import scipy.stats as stats
from sklearn.linear_model import LinearRegression, RidgeCV,LassoCV
from sklearn.metrics import mean_squared_error, mean_absolute_error
from sklearn.model_selection import train_test_split,KFold
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import RobustScaler
import xgboost as xgb
from sklearn.feature_extraction import DictVectorizer

def house_floor_v(house_floor):
    total = house_floor["total_floor"]
    if re.search("高层", house_floor["house_floor"]) is not None:
        return math.ceil((((total / 3) * 2 + total) / 2))
    elif re.search("中层", house_floor["house_floor"]) is not None:
        return math.ceil((((total / 3) * 1 + (total / 3) * 2) / 2))
    elif re.search("低层", house_floor["house_floor"]) is not None:
        return math.ceil((0 + (total / 3) * 1) / 2)
    elif re.search("地下", house_floor["house_floor"]) is not None:
        return 0
    else:
        return math.ceil(total/2)

def datahandle():
    pd.set_option('display.max_columns', 20)  # a就是你要设置显示的最大列数参数
    pd.set_option('display.max_rows', 100000)  # b就是你要设置显示的最大的行数参数
    pd.set_option('display.width', 1000)  # 就是你要设置显示的最大宽度
    plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签
    plt.rcParams['axes.unicode_minus'] = False  # 用来正常显示负号
    house_collection = pymongo.MongoClient().resoldHouse.houseinfo
    plot_collection = pymongo.MongoClient().resoldHouse.plotinfo
    house_list = list(house_collection.find({}, {'_id': 0}))
    plot_list = list(plot_collection.find({}, {'_id': 0}))
    house_frame = pd.DataFrame(house_list)
    plot_frame = pd.DataFrame(plot_list)
    # 先将房屋url和小区url规范化
    house_frame["house_url"] = house_frame["house_url"].str.extract("(\w+://[\w.]+[/\w+]+)\S+")
    house_frame["plot_url"] = house_frame["plot_url"].str.extract("(\w+://[\w.]+[/\w+]+)\S+")
    # 将重复的房屋信息去重
    house_frame.drop_duplicates(subset=["house_url"], inplace=True)
    # 将房屋信息和小区信息拼接起来
    data_frame = pd.merge(house_frame,plot_frame,how='left',on='plot_url')
    # print(data_frame.info())
    # 将一些可以用数值表示的列，去掉单位
    # print(data_frame["h_per_area_price"].str.contains("元/㎡").sum())
    # print(data_frame["h_total_area"].str.contains("m²").sum())
    # print(data_frame["h_total_price"].str.contains("万").sum())
    data_frame["h_per_area_price"] = data_frame["h_per_area_price"].str.extract("(\d+.*\d+)").astype("float64")
    data_frame["h_total_area"] = data_frame["h_total_area"].str.extract("(\d+.*\d+)").astype("float64")
    data_frame["h_total_price"] = data_frame["h_total_price"].str.extract("(\d+.*\d+)").astype("float64")

    #看一下这些值之间的协方差  结论:小区的总户数，租房数与房价成负相关
    # print(data_frame.corr()["h_total_price"])
    # print(data_frame[data_frame["h_per_area_price"].isnull()][["h_per_area_price","h_total_area","h_total_price"]])
    # 将h_per_area_price为空的数据删除
    data_frame.dropna(subset=["h_per_area_price"], inplace=True)
    # 填充h_total_price为空的数据  用h_per_area_price*h_total_area/10000填充
    # print(data_frame[data_frame["h_total_price"].isnull()][["h_per_area_price", "h_total_area"]])
    fill_price=data_frame[data_frame["h_total_price"].isnull()].h_per_area_price * data_frame[data_frame["h_total_price"].isnull()].h_total_area/10000
    data_frame["h_total_price"].fillna(fill_price,inplace=True)
    #直接将plot_url为空的数据删除
    data_frame.dropna(subset=["plot_url"], inplace=True)
     # 将含有空缺值的数据，从小到大排序，以条形图的方式展示

    # print(data_frame.info())
    null_count = data_frame.isnull().sum().sort_values()
    # print(null_count)
    # plt.figure(figsize=(18,15))
    # plt.bar(null_count[null_count>0].index,null_count[null_count>0])
    # plt.xticks(rotation='270')
    # for x,y in zip(null_count[null_count>0].index,null_count[null_count>0]):
    #     plt.text(x,y+6,y,ha='center')
    # # null_count[null_count>0].sort_values().plot.bar()
    # plt.savefig("./img/null_columns.png")
    # plt.show()
    # 对楼层进行处理   house_floor 直接取中层，低层，高层 由于楼层对房价的影响几乎没有，又存在这么多的nan，我们则不将其纳入进行预测的特征中
    # data_frame["house_floor"] = data_frame[["house_floor", "total_floor"]].apply(house_floor_v, axis=1)
    data_frame["total_floor"] = data_frame["house_floor"].str.extract("共(\d+)层").astype("float")
    data_frame["total_floor"].fillna(data_frame["total_floor"].mean(), inplace=True)
    data_frame["house_floor"] = data_frame["house_floor"].str.extract("(\S+)\(")
    # print(data_frame[data_frame["house_floor"].isnull()][["total_floor","house_floor"]])
    # print(data_frame["total_floor"].describe())
    # sns.boxplot(x="house_floor",y="h_total_price",data=data_frame[data_frame["total_floor"]>=15])
    # plt.title("总楼层大于15层")
    # plt.savefig("./img/floorup15.png")
    # sns.boxplot(x="house_floor", y="h_total_price", data=data_frame[data_frame["total_floor"] < 15])
    # plt.title("总楼层小于15层")
    # plt.savefig("./img/floordown15.png")
    # print(data_frame["total_floor"].corr(data_frame["h_total_price"]))
    # print(data_frame["house_floor"].corr(data_frame["h_total_price"]))
    # 对有无电梯进行处理  elevator  有：1 无 ：0
    data_frame.loc[data_frame["elevator"] == '有', "elevator"] = 1
    # data_frame.loc[(data_frame["elevator"].isna()) & (data_frame["total_floor"] > 8), "elevator"] = 1
    # data_frame.loc[(data_frame["elevator"].isna()) & (data_frame["total_floor"] <= 8), "elevator"] = 0
    data_frame["elevator"].fillna(0, inplace=True)
    # 先看一下电梯与楼层的相关性
    print(data_frame["total_floor"].corr(data_frame["elevator"]))

    # 对竣工年限进行处理  complete_date 将竣工年限为nan的填充为中位数
    # print(data_frame["complete_date"].isnull().sum())
    # data_frame["complete_date"] = data_frame["complete_date"].str.extract("(\d+)年").astype("float")
    # print(data_frame["complete_date"].isnull().sum())
    # print(data_frame["complete_date"].corr(data_frame["h_total_price"]))
    # plt.scatter("complete_date",'h_total_price', data=data_frame)
    # plt.xlabel("complete_date")
    # plt.ylabel("h_total_price")
    # plt.savefig("./img/complete_date.png")
    # data_frame["complete_date"].fillna(data_frame["complete_date"].median(), inplace=True)
    """
   

    
    # 对唯一住房进行处理  only_house 1:是 0:否
    data_frame.loc[data_frame["only_house"]=='是',"only_house"] = 1
    data_frame["only_house"].fillna(0,inplace=True)
    # data1=data_frame.dropna(subset=["only_house"],axis=0)
    # print(data1["h_total_price"].corr(data1["only_house"]))
    # print(data_frame["h_total_price"].corr(data_frame["only_house"]))
    # plt.ylim(0, 2000)   #设置y的坐标范围
    # sns.boxplot("only_house","h_total_price",data=data_frame)
    # 对房本年限进行处理  book_years
    # print(data_frame.loc[data_frame["book_years"].notna()])
    data_frame["book_years"] = data_frame["book_years"].str.extract("满(\S+)年")
    data_frame["book_years"] = data_frame["book_years"].apply(lambda x: 5 if x=='五' else 2)
    # plt.ylim(0, 3000)  # 设置y的坐标范围
    # sns.boxplot("book_years", "h_total_price", data=data_frame)
    # plt.savefig("./img/book_years.png")
    data_frame["book_years"].fillna("房本年限满二年",inplace=True)
    # 对产权年限进行处理  rights_year  （住宅, 别墅, 排屋, 平房为70，商住楼, 公寓, 其他为40） 先将产权年限暂无的改为nan
    data_frame.loc[data_frame["rights_year"] == "产权年限暂无", "rights_year"] = np.nan
    data_frame.loc[(data_frame["rights_year"].isna()) & ((data_frame["house_sort"] == "普通住宅") | (data_frame["house_sort"] == "别墅")
              | (data_frame["house_sort"] == "排屋") | (data_frame["house_sort"] == "平房")), "rights_year"] = "产权年限70年产权"
    data_frame.loc[(data_frame["rights_year"].isna()) & ((data_frame["house_sort"] == "商住楼") | (data_frame["house_sort"] == "公寓")
              | (data_frame["house_sort"] == "其他")), "rights_year"] = "产权年限40年产权"
    data_frame["rights_year"] = data_frame["rights_year"].str.extract("(\d+)")
    plt.figure(figsize=(18, 12))
    # plt.ylim(0, 4000)  # 设置y的坐标范围
    # sns.violinplot(x="rights_year", y="h_total_price", data=data_frame)
    # plt.savefig("./img/rights_year.png")
    # 为啥不行？？？？？
    # print(data_frame["rights_year"].corr(data_frame["h_total_price"]))
    # p_per_area_price 的赋值
    # plt.figure(figsize=(18,12))
    # p_per_area_price_img = sns.jointplot(x="p_per_area_price", y="h_per_area_price", data=data_frame, kind='reg')
    # p_per_area_price_img.ax_joint.legend(stats.pearsonr(data_frame.loc[data_frame["p_per_area_price"].notna(),"p_per_area_price"],data_frame.loc[data_frame["p_per_area_price"].notna(),"h_per_area_price"]))
    # plt.savefig("./img/p_per_area_price.png")
    p_per_area_price_null_data = data_frame["p_per_area_price"].isnull()
    data_frame.p_per_area_price[p_per_area_price_null_data] = data_frame.h_per_area_price[p_per_area_price_null_data]*0.97
    # rent_house_num,total_house_num都以中位数进行的填充
    # print(data_frame["total_house_num"].corr(data_frame["h_total_price"]))
    # sns.jointplot(x=data_frame["second_house_num"],y=data_frame["h_total_price"],kind='reg')
    # sns.displot(data_frame["rent_house_num"])
    data_frame["total_house_num"].fillna(data_frame["total_house_num"].median(),inplace=True)
    data_frame["rent_house_num"].fillna(data_frame["rent_house_num"].median(),inplace=True)
    # property_price 以每一年的竣工年限的中位数进行填充
    data_frame.drop(data_frame[data_frame["complete_date"]<1960].index,inplace=True)
    data_frame.loc[data_frame["property_price"]>25,"property_price"]=3.5
    # plt.ylim(0, 12.5)
    # plt.xlim(1960, 2020)
    # sns.jointplot(x=data_frame["complete_date"], y=data_frame["property_price"], kind='reg')
    # plt.savefig("./img/property_price.png")
    # property_price的填充
    # data_frame.isnull(subset=["property_price"])
    # data_frame.loc[data_frame["property_price"].isna(),["complete_date","property_price"]].fillna(lambda x: v.value if x.complete_date == v.index else 0,inplace=True)
    # print(type(data_frame.groupby("complete_date")["property_price"]))
    # data_frame.applymap(lambda x:print(x))

    # v = data_frame.groupby("complete_date")["property_price"].mean()
    # print(v.index,v.values)
    # data_frame["property_price"].fillna(data_frame.apply(lambda row:v.values if row["complete_date"]== v.index else 0),inplace=True,axis=1)
    # data_frame["property_price"].fillna(data_frame.groupby("complete_date")["property_price"].mean(),inplace=True)

    # plt.ylim(0, 2.5)
    # sns.jointplot(x="business_circle",y="property_price",data=data_frame[data_frame["property_price"]<2.5])


    # 特征值中要去掉的列 最后两个列要填充的
    drop_columns = ["house_url", "plot_url", "p_name", "p_address", "business_circle","house_floor","second_house_num","green_rate_level","property_company","p_total_area","around_school","park_spot_num","property_price","green_rate_num"]
    data_frame = data_frame.drop(columns=drop_columns, axis=1)
    null_count = data_frame.isnull().sum().sort_values()
    print(null_count[null_count > 0])
    # print(data_frame.corr())
    plt.show()
    # data_frame.to_csv("./data_frame.csv",encoding="utf-8_sig")
    """



def machineLeaning():
    # 先进行one-hot编码
    features = pd.read_csv("./data_frame.csv")
    # print(features.info())
    """
    print(features.info())
    plt.figure(figsize=(20,18))
    sns.heatmap(features.corr(),linewidth=.8,annot=True)
    plt.savefig("./img/covariance2.png")
    plt.show()
     """
    target = features["h_total_price"] * 10000
    # features = pd.get_dummies(features.drop(columns=["h_total_price","green_rate_num","property_price"],axis=1))
    features = features.drop(columns=["h_total_price","green_rate_num","property_price"],axis=1)
    vectorize = DictVectorizer()
    features = vectorize.fit_transform(features.to_dict(orient="records"))
    # 拆分训练集和测试集
    X_train, X_test, y_train, y_test = train_test_split(features, target, test_size=0.25)
    # 先用基础线性模型
    lm = LinearRegression()
    lmmoldel = lm.fit(X_train,y_train)
    Benchmark(lmmoldel,X_test,y_test,"LinearRegression")
    # 使用 RidgeRegression
    # kfold = KFold(n_splits=2,shuffle=True,random_state=123)
    # alphas = np.logspace(-10,2,200)
    # rc = RidgeCV(alphas=alphas)
    # rcmoldel = make_pipeline(RobustScaler(),rc).fit(X_train,y_train)
    # Benchmark(rcmoldel,X_test,y_test,"RidgeRegression")
    # 使用XGBRegressor
    # matrix = xgb.DMatrix(data=X_train,label=y_train)
    # xg_reg = xgb.XGBRegressor(objective="reg:linear",colsample_bytree=0.7,learning_rate=0.01,max_depth=3,estimators=3460,subsample=0.7,reg_alpha=0.0006)
    # xg_reg.fit(X_train,y_train)
    # Benchmark(xg_reg,X_test,y_test,"XGBRegressor")
    # print(y_test.iloc[0])

def Benchmark(moldel,X_test,y_test,modelname):
    y_predict = moldel.predict(X_test)
    # print(y_predict+","+y_test.iloc[0])
    print(modelname + "均方误差 :" , mean_squared_error(y_predict, y_test))
    print(modelname + "对数均方误差" ,np.sqrt(mean_absolute_error(np.log(y_predict), np.log(y_test))))


if __name__ == '__main__':
    datahandle()
    # machineLeaning()