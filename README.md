## 项目介绍
- 爬取安居客二手房数据，对某个二手房价格做出合理预测
- 开发成员：cyy、sqyhuya、cao_xiong_neng

## 工具/技术
- pycharm、mongodb、mysql、python

## 目录说明
- dataProcess : 数据处理
  - housePlotProcess.py  数据分析可视化
- houseCrawler : 数据爬虫
  - selenium : selenium实现数据爬取
    - houseSpiderSelenium.py 爬取杭州二手房
    - plotSpiderSelenium.py  爬取杭州小区
  - spiders : scrapy实现数据爬取
- static : 静态资源
- utils : 工具类
- properties.py : 获取数据库连接（mongo、mysql）
- README.md : 项目说明文件




    
  

  
   
