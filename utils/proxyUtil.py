import telnetlib
import requests


def verify_proxy(proxy):
    """
    验证代理是否有效
    proxy 219.151.142.29:3128
    """
    verify = False
    try:
        # 验证安居客
        proxy_json = {'http': 'http://' + proxy,
                      'https': 'https://' + proxy
                      }
        res = requests.get('http://www.baidu.com/', proxies=proxy_json, timeout=8)
        # 验证telnet
        proxy_split = proxy.split(":")
        telnetlib.Telnet(proxy_split[0], proxy_split[1], timeout=8)
        if len(res.content) >1000:
            verify = True
        else:
            verify = False
    except:
        verify = False
    return verify
