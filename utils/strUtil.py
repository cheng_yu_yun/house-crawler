import re


def remove_spaces(str):
    """去除一切空格、回车、制表符、换行"""
    try:
        str = re.sub(r'\\r|\\t|\\n', '', str).strip()
    except:
        print("remove_spaces--"+str+"error")
        str = ""
    return str

def cnum_to_anum(str):
    """ 汉语数字与阿拉伯数字转换 """
    dict = {"一":1,
            "二":2,
            "三":3,
            "四":4,
            "五":5,
            "六":6,
            "七":7,
            "八":8,
            "九":9
            }
    return dict[str]